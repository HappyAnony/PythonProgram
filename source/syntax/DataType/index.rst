数据类型
=========================

数据类型申明

- 对象的存储方式(是否可变等属性)
- 对象所支持的操作

.. toctree::
   :titlesonly:
   :glob:


   Bool/index
   Numeric/index
   Sequence/index
   Set/index
   Map/index
   File/index
   Iterator/index
   Generator/index
   Others/index
