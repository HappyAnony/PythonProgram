.. Python Programming documentation master file, created by
   sphinx-quickstart on Fri Jun 22 15:16:23 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Python Programming's documentation!
==============================================

参考文档：\ `awesomePythonCn <https://github.com/HappyAnony/awesome-python-cn>`_\ 



.. toctree::
   :titlesonly:
   :glob:


   preface/index
   syntax/index
   library/index
   datastruct/index
   algorithm/index
   framework/index
   developtool/index
